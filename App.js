import React from 'react';
//import redux libraries
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
//import react-navigations libraries and component
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import rootReducer from './src/store/reducers/rootReducer';
//import route components
import { ImageDetails } from './src/components/ImageDetails';
import { MainScreen } from './src/container/MainScreen';

const store = createStore(rootReducer, applyMiddleware(thunk));

const Stack = createStackNavigator();

export default function App() {

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={MainScreen} />
          <Stack.Screen name="Details" component={ImageDetails} 
            options={({route})=> ({title: route.params.name || 'Details'})}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
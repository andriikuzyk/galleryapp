import { 
    FETCH_ITEMS_START, 
    FETCH_ITEMS_SUCCESS, 
    FETCH_ITEMS_ERROR 
} from './actionTypes';

export const fetchItems = () => {
    return async dispatch => {
        try {
            const response = await fetch(
              "https://api.unsplash.com/photos/?client_id=896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043"
            );
            const data = await response.json()
               dispatch(fetchItemsSuccess(data)); 
        } catch (err) {
            dispatch(fetchItemsError(err))
        }
    }
}

export const fetchItemsStart = () => {
    return {
        type: FETCH_ITEMS_START
    }
}

export const fetchItemsSuccess = items => {
    return {
        type: FETCH_ITEMS_SUCCESS,
        items
    }
}

export const fetchItemsError = err => {
    return {
        type: FETCH_ITEMS_ERROR,
        err
    }
}

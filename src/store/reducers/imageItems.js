import {
  FETCH_ITEMS_SUCCESS,
  FETCH_ITEMS_ERROR,
} from "../actions/actionTypes";

const initialState = {
    items: [],
    loading: true,
    error: null
}

export const itemsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ITEMS_SUCCESS:
            return {
                ...state, loading: false, items: action.items
            }
        case FETCH_ITEMS_ERROR:
            return {
                ...state, loading: false, error: action.err
            }
        default:
            return state
    }
}
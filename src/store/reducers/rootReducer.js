import { combineReducers } from 'redux';
import { itemsReducer } from './imageItems';

export default combineReducers({
    items: itemsReducer
})
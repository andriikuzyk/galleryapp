import React, { useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ImagesItem } from '../components/ImagesItem';
import { fetchItems } from '../store/actions/imageItems';
import { styles } from '../styles/mainScreen';

export const MainScreen = () => {

    const data = useSelector(state => state.items.items);

    const loading = useSelector(state => state.loading)

    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(fetchItems());
    }, []);

    return (
      <ScrollView>
        <View style={styles.mainScreen}>
          {data.map((item) => {
            return (
              <ImagesItem
                key={item.id}
                userName={item.user.name}
                userThumbImg={item.urls.thumb}
                userFullImg={item.urls.raw}
                description={item.description}
              />
            );
          })}
        </View>
      </ScrollView>
    );
}
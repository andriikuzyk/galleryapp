import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({

  previevImage: {
    width: 160,
    height: 160,
  },
  description: {
    textAlign: "center",
    fontSize: 14,
    width: 160,
    color: "#C1BAC1",
  },
  userName: {
    textAlign: "center",
    color: "#fff",
    width: 160,
    fontSize: 18,
    marginTop: 10,
    color: "#3c3739",
    paddingBottom: 10
  },
});

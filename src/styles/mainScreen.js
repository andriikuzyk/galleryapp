import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  mainScreen: {
    width: "100%",
    paddingTop: 15,
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around",
  },
});

import React from 'react';
import { Text,Image,TouchableOpacity } from 'react-native';
import { useNavigation } from "@react-navigation/native";
import { styles } from '../styles/imageItem';

export const ImagesItem = ({ userName, userThumbImg, userFullImg, description }) => {
  const navigation = useNavigation();
  
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.push("Details", {
          userFullImg,
          name: userName,
        })
      }
    >
      <Image
        style={styles.previevImage}
        source={{
          uri: `${userThumbImg}`,
        }}
      />      
      <Text style={styles.description}>{description}</Text>
      <Text style={styles.userName}>{userName}</Text>
    </TouchableOpacity>
  );
};
import React from 'react';
import { View } from "react-native";
import { useRoute } from "@react-navigation/native";

import FitImage from "react-native-fit-image";

export const ImageDetails = () => {
    const {params} = useRoute()

    return (
        <View style={{flex: 1, justifyContent: 'center'}}>
            <FitImage
                indicator={true} 
                indicatorColor="#000"
                indicatorSize="large"
                originalWidth={600}
                originalHeight={400}
                source={{ uri: `${params.userFullImg}` }}
            />
        </View>
    
    );
}